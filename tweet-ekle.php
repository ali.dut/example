<?php 
include("sabit/ustlink.php");
include("inc/yetkikontrol.php");
$gelenYetki =$_SESSION['yetki'];
$yetki_seviyesi = yetkiKontrol($gelenYetki);
if ($yetki_seviyesi < 5)
{
  header("location:yetkisizerisim.php");
  exit;
}
?>
<!-- Sayfaya özgü linkler -->
 <!-- Select2 -->
 <link rel="stylesheet" href="eklenti/select2/css/select2.min.css">
 <link rel="stylesheet" href="eklenti/select2-bootstrap4-theme/select2-bootstrap4.min.css">
<!-- Sayfaya özgü linkler bitiş -->
<?php 
include("sabit/ust.php");
include("sabit/sol.php");
?>

<!-- İçerik Bölümü -->
<div class="content-wrapper">
    <!-- İçerik Başlık -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Tweet Ekle</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="tweet-listele.php">Tweet Listesi</a></li>
              <li class="breadcrumb-item active">Tweet Ekle</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.İçerik Başlık Bitiş -->
    <!-- İçerik Konu  -->
    <section class="content">
		<div class="row">
          <div class="card card-outline card-info col-sm-6">
                  <div class="card-header">
                      <h3 class="card-title">Tweet Ekle</h3>

                      <div class="card-tools">
                      <!-- <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                          <i class="fas fa-minus"></i></button> --> 
                          <!-- Küçültme butonu gerek olursa açarız sonra -->
                      </div>
                  </div>
                  <div class="card-body">
                      <div class="row">
                          <div class="form-group col-sm-12">
                              <label for="kategori_id">Kategori  (*)</label>
                              <select id="kategori_id" name="kategori_id" class="form-control select2bs4" style="width: 100%;">
                              </select>
                          </div> 
                          <div class="form-group col-sm-12">
                              <label for="dizifilm_id">Dizi/Film  (*)</label>
                              <select id="dizifilm_id" name="dizifilm_id" class="form-control select2bs4" style="width: 100%;">
                              </select>
                          </div> 
                          <div class="form-group col-sm-12">
                              <label for="tur_id">Tür  (*)</label>
                              <select id="tur_id" name="tur_id" class="form-control select2bs4" style="width: 100%;">
                              </select>
                          </div>
                          <div class="form-group col-sm-12">
                              <label for="icerik">İçerik</label>
                              <textarea id="icerik" maxlength="1000" name="icerik"  onkeyup="countChar(this)" class="form-control" rows="3"></textarea>
                              <div class="row">
                                  <div class="col-lg-8">
                                      <button type="button" class="btn btn-primary btn-sm" name="hashtag" id="addHashtag"> Etiket ekle </button>
                                      <button type="button" class="btn btn-primary btn-sm" name="hashtagWoman" id="addhashtagWoman"> Kadın </button>
                                      <button type="button" class="btn btn-primary btn-sm" name="hashtagMan" id="addhashtagMan"> Erkek </button>
                                      <button type="button" class="btn btn-primary btn-sm" name="hashtagCamera" id="addhashtagCamera"> Kamera </button>
                                      <button type="button" class="btn btn-primary btn-sm" name="hashtagMoon" id="addhashtagMoon"> Ay </button>
                                      <button type="button" class="btn btn-primary btn-sm" name="hashtagSun" id="addhashtagSun"> Gunes </button>
                                      <button type="button" class="btn btn-primary btn-sm" name="hashtagTime" id="addhashtagTime"> Saat </button>
									  <button type="button" class="btn btn-primary btn-sm" name="hashtagKalp" id="addhashtagKalp"> Kalp </button>
                                  </div>
                                    <label class="col-lg-1">Boyut:</label>
                                    <label class="col-lg-1" id="demo">0</label>
                              </div>
                          </div>    
                          <div class="row">
                              <div class="form-group col-sm-6">
                                  <label for="yukle1">Resim 1</label>
                                  <input type="FILE" name="yukle1" id="yukle1"  accept="image/jpg, image/jpeg, image/png" >
                                  <input id="resim1" name="resim1" type="text" class="form-control" >
                              </div> 
                              <div class="form-group col-sm-6">
                                  <label for="yukle2">Resim 2</label>
                                  <input type="FILE" name="yukle2" id="yukle2"  accept="image/jpg, image/jpeg, image/png" >
                                  <input id="resim2" name="resim2" type="text" class="form-control" >
                              </div>  
                              <div class="form-group col-sm-6">
                                  <label for="yukle3">Resim 3</label>
                                  <input type="FILE" name="yukle3" id="yukle3"  accept="image/jpg, image/jpeg, image/png" >
                                  <input id="resim3" name="resim3" type="text" class="form-control" >
                              </div>  
                              <div class="form-group col-sm-6">
                                  <label for="yukle4">Resim 4</label>
                                  <input type="FILE" name="yukle4" id="yukle4"  accept="image/jpg, image/jpeg, image/png" >
                                  <input id="resim4" name="resim4" type="text" class="form-control" >
                              </div> 
                          </div>
                          <div class="col-sm-5 form-group">
                              <label for="skt">Son Kullanım Tarihi <?php $dateBack = new DateTime(); $dateBack ->modify('-7 month'); $dateBack = $dateBack ->format('Y-m-d H:i:s'); $dateTimebugun = $dateBack;  $dateTimebugun = str_replace(" " , "T" ,$dateTimebugun);  ?></label>
                              <div class="input-group">
                                  <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                  </div>
                                  <input id="skt" name="skt" type="datetime-local" value="<?php echo $dateTimebugun; ?>" class="form-control" >
                              </div> 
                          </div> 
                          <div class="col-sm-3 form-group">
                              <label for="sayi">Kullanım Sayısı</label>
                              <input id="sayi" name="sayi" type="number" class="form-control" value="0" >
                          </div>   
                          <div class="col-sm-4 form-group">
                              <label for="durum">Durum</label>
                              <select id="durum" name="durum" class="form-control select2bs4" style="width: 100%;">
                                  <option>Aktif</option>
                                  <option>Pasif</option>
                              </select>
                          </div> 
                          <button type="submit" id="kaydet" name="kaydet" class="btn btn-success btn-block"  >Kaydet</button>  
                      </div> 
                  </div>
                  <!-- /.card-body -->
          </div>
			  <div class="card card-outline card-warning col-sm-6">
					  <div class="card-header">
						  <h3 class="card-title">Dizi/Film Ekle</h3>
					  </div>
					  <div class="card-body">
							  <div class="form-group">
								  <label for="adi">Adı</label>
								  <div class="row">
									<div class="col-sm-9">
										<input type="text"  id="adi" name="adi" class="form-control">
									</div>
									<div class="col-sm-3">
										<button type="submit"  id="dizikaydet" name="dizikaydet"  class="btn btn-success btn-block"  style="margin-right:15px; width:100px; float:right">Kaydet</button>  
									</div>
								  </div>
							  </div>   						  
					  </div>
					  <!-- /.card-body -->
			  </div>
		</div>
    </section>
    <!-- /.İçerik Konu Bitiş -->

</div>
<!-- ./İçerik Bölümü Bitiş -->
<?php 
include("sabit/alt.php");
?>
<!-- Sayfaya özgü scriptler -->

<!-- Select2 -->
<script src="eklenti/select2/js/select2.full.min.js"></script>
<script>
  $(function () {
    $('.select2').select2();
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    });
    $("#kategori_id").select2({
        minimumInputLength: 0,
        language: "tr",
        width: 'resolve',
        containerCss : {"display":"block"},
        dropdownAutoWidth : true,
        placeholder: "Seçim yapınız",
        ajax: {
            url: 'ajax/cek-kategori.php',
            dataType: 'json',
            type: "POST",
            data: function (term) {
                return {
                    term: term
                };
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.adi,
                            id: item.id
                        }
                    })
                };
            }

          }
      });
      $("#tur_id").select2({
        minimumInputLength: 0,
        language: "tr",
        width: 'resolve',
        containerCss : {"display":"block"},
        dropdownAutoWidth : true,
        placeholder: "Seçim yapınız",
        ajax: {
            url: 'ajax/cek-tur.php',
            dataType: 'json',
            type: "POST",
            data: function (term) {
                return {
                    term: term
                };
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.adi,
                            id: item.id
                        }
                    })
                };
            }

          }
      });
      $("#dizifilm_id").select2({
        minimumInputLength: 1,
        language: "tr",
        width: 'resolve',
        containerCss : {"display":"block"},
        dropdownAutoWidth : true,
        placeholder: "Seçim yapınız",
        ajax: {
            url: 'ajax/cek-dizifilm.php',
            dataType: 'json',
            type: "POST",
            data: function (term) {
                return {
                    term: term
                };
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.adi,
                            id: item.id
                        }
                    })
                };
            }

          }
      });
      $("#yukle1").change(function(e){//her hangi bir şey seçilirse
          e.preventDefault();
              const Toast = Swal.mixin({
                            toast: true,
                            position: 'top-end',
                            showConfirmButton: false,
                            timer: 3000
                          });
          var resim=document.getElementById ("yukle1");//resime eriş
          if (resim.files && resim.files[0]){//veri var mı kontrol ediyoruz.
            var veri=new FileReader();//apiyi başlatıyoruz.
            veri.onload=function() {//aşağıda dosya verisini okuyoruz.Eğer veri okunmuşsa
              var resimveri=veri.result;//veriyi al
                $.post("ajax/resimyukle.php",
                {"veri":resimveri},
                function(resim, status, jqXHR){//kayit.php yolluyoruz.

                  var jsonData = JSON.parse(resim);
                  if (jsonData.success == 1)
                  {
                    Toast.fire({
                                icon: jsonData.icon,
                                title: jsonData.adi
                    });
                    window.onbeforeunload = function() { return "are you sure?"; }
                  $("#resim1").val(jsonData.resimadi)  ;
                  }
                  else
                  {
                    Toast.fire({
                                icon: jsonData.icon,
                                title: jsonData.adi
                    });
                  }
                })
            }
            veri.readAsDataURL(resim.files[0]);//veriyi okuyoruz.
          }
      });
      $("#yukle2").change(function(e){//her hangi bir şey seçilirse
          e.preventDefault();
              const Toast = Swal.mixin({
                            toast: true,
                            position: 'top-end',
                            showConfirmButton: false,
                            timer: 3000
                          });
          var resim=document.getElementById ("yukle2");//resime eriş
          if (resim.files && resim.files[0]){//veri var mı kontrol ediyoruz.
            var veri=new FileReader();//apiyi başlatıyoruz.
            veri.onload=function() {//aşağıda dosya verisini okuyoruz.Eğer veri okunmuşsa
              var resimveri=veri.result;//veriyi al
                $.post("ajax/resimyukle.php",
                {"veri":resimveri},
                function(resim, status, jqXHR){//kayit.php yolluyoruz.

                  var jsonData = JSON.parse(resim);
                  if (jsonData.success == 1)
                  {
                    Toast.fire({
                                icon: jsonData.icon,
                                title: jsonData.adi
                    });
                    window.onbeforeunload = function() { return "are you sure?"; }
                  $("#resim2").val(jsonData.resimadi)  ;
                  }
                  else
                  {
                    Toast.fire({
                                icon: jsonData.icon,
                                title: jsonData.adi
                    });
                  }
                })
            }
            veri.readAsDataURL(resim.files[0]);//veriyi okuyoruz.
          }
      });
      $("#yukle3").change(function(e){//her hangi bir şey seçilirse
          e.preventDefault();
              const Toast = Swal.mixin({
                            toast: true,
                            position: 'top-end',
                            showConfirmButton: false,
                            timer: 3000
                          });
          var resim=document.getElementById ("yukle3");//resime eriş
          if (resim.files && resim.files[0]){//veri var mı kontrol ediyoruz.
            var veri=new FileReader();//apiyi başlatıyoruz.
            veri.onload=function() {//aşağıda dosya verisini okuyoruz.Eğer veri okunmuşsa
              var resimveri=veri.result;//veriyi al
                $.post("ajax/resimyukle.php",
                {"veri":resimveri},
                function(resim, status, jqXHR){//kayit.php yolluyoruz.

                  var jsonData = JSON.parse(resim);
                  if (jsonData.success == 1)
                  {
                    Toast.fire({
                                icon: jsonData.icon,
                                title: jsonData.adi
                    });
                    window.onbeforeunload = function() { return "are you sure?"; }
                  $("#resim3").val(jsonData.resimadi)  ;
                  }
                  else
                  {
                    Toast.fire({
                                icon: jsonData.icon,
                                title: jsonData.adi
                    });
                  }
                })
            }
            veri.readAsDataURL(resim.files[0]);//veriyi okuyoruz.
          }
      });
      $("#yukle4").change(function(e){//her hangi bir şey seçilirse
          e.preventDefault();
              const Toast = Swal.mixin({
                            toast: true,
                            position: 'top-end',
                            showConfirmButton: false,
                            timer: 3000
                          });
          var resim=document.getElementById ("yukle4");//resime eriş
          if (resim.files && resim.files[0]){//veri var mı kontrol ediyoruz.
            var veri=new FileReader();//apiyi başlatıyoruz.
            veri.onload=function() {//aşağıda dosya verisini okuyoruz.Eğer veri okunmuşsa
              var resimveri=veri.result;//veriyi al
                $.post("ajax/resimyukle.php",
                {"veri":resimveri},
                function(resim, status, jqXHR){//kayit.php yolluyoruz.

                  var jsonData = JSON.parse(resim);
                  if (jsonData.success == 1)
                  {
                    Toast.fire({
                                icon: jsonData.icon,
                                title: jsonData.adi
                    });
                    window.onbeforeunload = function() { return "are you sure?"; }
                  $("#resim4").val(jsonData.resimadi)  ;
                  }
                  else
                  {
                    Toast.fire({
                                icon: jsonData.icon,
                                title: jsonData.adi
                    });
                  }
                })
            }
            veri.readAsDataURL(resim.files[0]);//veriyi okuyoruz.
          }
      });
  })
</script>
<script>
    function countChar(val) {
      var len = val.value.length;
      $('#demo').text(len);
    };
    //Emoji ekleme

    // tweet içeriğine hashtag eklenme işlemleri
    $('#addhashtagWoman').click(function (e) {
        var tag =  '👩';
        var tweet =  $('#icerik').val() + tag;
        $('#icerik').val(tweet);
        e.preventDefault();
    });
	$('#addhashtagKalp').click(function (e) {
        var tag =  '❤️';
        var tweet =  $('#icerik').val() + tag;
        $('#icerik').val(tweet);
        e.preventDefault();
    });

    $('#addhashtagMan').click(function (e) {
        var tag =  '👨🏻';
        var tweet =  $('#icerik').val() + tag;
        $('#icerik').val(tweet);
        e.preventDefault();
    });
    $('#addhashtagCamera').click(function (e) {
        var tag =  '🎥';
        var tweet =  $('#icerik').val() + tag;
        $('#icerik').val(tweet);
        e.preventDefault();
    });
    $('#addhashtagMoon').click(function (e) {
        var tag =  '🌙';
        var tweet =  $('#icerik').val() + tag;
        $('#icerik').val(tweet);
        e.preventDefault();
    });
    $('#addhashtagSun').click(function (e) {
        var tag =  '☀';
        var tweet =  $('#icerik').val() + tag;
        $('#icerik').val(tweet);
        e.preventDefault();
    });
    $('#addhashtagTime').click(function (e) {
        var tag =  '🕒';
        var tweet =  $('#icerik').val() + tag;
        $('#icerik').val(tweet);
        e.preventDefault();
    });
    // tweet içeriğine hashtag eklenme işlemleri
    $('#addHashtag').click(function (e) {
        var tag =  $("[id='dizifilm_id'] option:selected").text().trim().replace(/\s+/g, "")
        .replace("'","")
        .replace(":","")
        .replace(",","")
        .replace("\;","")
        .replace(".","");
        if ($('#kategori_id').val() === '1'){
            tag = '📺 #' + tag;
        }
        else if ($('#kategori_id').val() === '2'){
            tag = '🎬 #' + tag;
        }
        else {
            tag = '#' + tag;
        }
        var tweet =  $('#icerik').val() + '\n' +tag;
        $('#icerik').val(tweet);
        e.preventDefault();
      });
</script>
<script type="text/javascript">
$(document).ready(function() {
  $('#kaydet').click(function(e) {
      e.preventDefault();
      const Toast = Swal.mixin({
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 3000
                  });

      // Veriler toplanacak
      var kategori_iddata = $('#kategori_id').select2('data')
      var kategori_id = kategori_iddata[0].id;
      var dizifilm_iddata = $('#dizifilm_id').select2('data')
      var dizifilm_id = dizifilm_iddata[0].id;
      var tur_iddata = $('#tur_id').select2('data')
      var tur_id = tur_iddata[0].id;
      var icerik= $("#icerik").val();
      var resim1= $("#resim1").val();
      var resim2= $("#resim2").val();
      var resim3= $("#resim3").val();
      var resim4= $("#resim4").val();
      var skt = $("#skt").val();
      var sayi= $("#sayi").val();
      var durumdata = $('#durum').select2('data')
      var durum = durumdata[0].text;
      $.post('ajax/tweet-ekle.php',   // url
      {
        kategori_id: kategori_id,
        dizifilm_id:dizifilm_id,
        tur_id: tur_id,
        icerik:icerik,
        resim1: resim1,
        resim2:resim2,
        resim3:resim3,
        resim4:resim4,
        skt:skt,
        sayi:sayi,
        durum: durum,
      },
      function(data, status, jqXHR) 
      {// success callback
        var jsonData = JSON.parse(data);
        if(jsonData.success == 1)
        {
          Toast.fire({
                      icon: jsonData.icon,
                      title: jsonData.adi
          })
          window.onbeforeunload = function() { return null; }
          setTimeout("location.href = 'tweet-listele.php'",2000);
        }
        else 
        {
          Toast.fire({
                      icon: jsonData.icon,
                      title: jsonData.adi
          })
        }
      })    
    });
	  $('#dizikaydet').click(function(e) {
      e.preventDefault();
      const Toast = Swal.mixin({
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 3000
                  });

      // Veriler toplanacak
	  var adi= $("#adi").val();

      $.post('ajax/dizifilm-ekle.php',   // url
      {
        adi: adi,
      },
      function(data, status, jqXHR) 
      {// success callback
		var jsonData = JSON.parse(data);
		// user is logged in successfully in the back-end
		// let's redirect
		if (jsonData.success == "1")
		{
		  Toast.fire({
			  icon: 'success',
			  title: jsonData.adi
			})
			$("#adi").val("");
		}
		else if (jsonData.success == "3")
		{
		  Toast.fire({
			icon: 'info',
			title: jsonData.adi
		  })
		}
		else if (jsonData.success == "4")
		{
		  Toast.fire({
			icon: 'warning',
			title: jsonData.adi
		  })
		}
		else if (jsonData.success == "5")
		{
		  Toast.fire({
			icon: 'warning',
			title: jsonData.adi
		  })
		}
		else
		{
		  Toast.fire({
			icon: 'error',
			title: jsonData.adi
		  })
		}
      })    
    });
});
</script>

<!-- Sayfaya özgü linkler bitiş -->
<?php
include("sabit/altscript.php");
?>