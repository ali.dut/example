<?php 
        include ("../inc/head.php");

        if ($_SESSION['adi']==null||  $_SESSION['adi'] == "")
        {
            header("location:../giris.php");
            exit;
        }

        if($_POST)
        {
            $id =$_POST["id"];
            $adi =$_POST["adi"];
            $kim = $_SESSION['adi'] .'  id: '.$_SESSION['id'] ; 
            $dateTime = date("Y-m-d H:i:s");

            if(empty($adi) || $adi == null)
            {
                echo json_encode(array('success' => 3, 'adi' =>'Lütfen boş alanları doldurun.'));
            }
            else
            {
                $kategori = new kategoriler();
                $kategori ->id = $id;
                $kategori ->adi =$adi;
                $kategori ->_updatedby = $kim;
                $kategori ->_updateTime = $dateTime;
                $sonuc = $kategori->kategoriDuzenle();
                if($sonuc)
                    echo json_encode(array('success' => 1, 'adi' =>'Kategori düzenleme işlemi başarıyla gerçekleşti.'));
                else
                    echo json_encode(array('success' => 2, 'adi' =>'Kategori düzenlerken bir hata ile karşılaşıldı.'));
               
            }
        }
?>