<?php 
        include ("../inc/head.php");

        if ($_SESSION['adi']==null||  $_SESSION['adi'] == "")
        {
            header("location:../giris.php");
            exit;
        }

        if($_POST)
        {
            $adi =$_POST["adi"];
            $kim = $_SESSION['adi'] .'  id: '.$_SESSION['id'] ; 
            $dateTime = date("Y-m-d H:i:s");

            if(empty($adi) || $adi == null)
            {
                echo json_encode(array('success' => 3, 'adi' =>'Lütfen boş alanları doldurun.'));
            }
            else
            {
                $dizifilm = new dizifilm();
                $dizifilm ->adi =$adi;
                $dizifilm ->_createdby = $kim;
                $dizifilm ->_createTime = $dateTime;
                $dizifilm ->_deleted = 0;
                $sonuc = $dizifilm->dizifilmEkle();
                if($sonuc)
                    echo json_encode(array('success' => 1, 'adi' =>'Dizi/Film ekleme işlemi başarıyla gerçekleşti.'));
                else
                    echo json_encode(array('success' => 2, 'adi' =>'Dizi/Film eklerken bir hata ile karşılaşıldı.'));
               
            }
        }
?>