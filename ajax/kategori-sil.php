<?php 
        include ("../inc/head.php");

        if ($_SESSION['adi']==null||  $_SESSION['adi'] == "")
        {
            header("location:../giris.php");
            exit;
        }

        if($_POST)
        {
            $id =$_POST["id"];
            $kim = $_SESSION['adi'] .'  id: '.$_SESSION['id'] ; 
            $dateTime = date("Y-m-d H:i:s");
            $kategori = new kategoriler();
            $kategori ->id =$id;
            $kategori ->_deleted =1;
            $kategori ->_deletedby = $kim;
            $kategori ->_deletedTime = $dateTime;
            $sonuc = $kategori->kategoriSil();
            if($sonuc)
                echo json_encode(array('success' => 1, 'adi' =>'Kategori silme işlemi başarıyla gerçekleşti.'));
            else
                echo json_encode(array('success' => 2, 'adi' =>'Kategori silinirken bir hata ile karşılaşıldı.'));
           
        }
?>