<?php 
        include ("../inc/head.php");

        if ($_SESSION['adi']==null||  $_SESSION['adi'] == "")
        {
            header("location:../giris.php");
            exit;
        }

        if($_POST)
        {
            $adi =$_POST["adi"];
            $kim = $_SESSION['adi'] .'  id: '.$_SESSION['id'] ; 
            $dateTime = date("Y-m-d H:i:s");

            if(empty($adi) || $adi == null)
            {
                echo json_encode(array('success' => 3, 'adi' =>'Lütfen boş alanları doldurun.'));
            }
            else
            {
                $kategori = new kategoriler();
                $kategori ->adi =$adi;
                $kategori ->_createdby = $kim;
                $kategori ->_createTime = $dateTime;
                $kategori ->_deleted = 0;
                $sonuc = $kategori->kategoriEkle();
                if($sonuc)
                    echo json_encode(array('success' => 1, 'adi' =>'Kategori ekleme işlemi başarıyla gerçekleşti.'));
                else
                    echo json_encode(array('success' => 2, 'adi' =>'Kategori eklerken bir hata ile karşılaşıldı.'));
               
            }
        }
?>