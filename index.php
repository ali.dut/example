<?php 
include("sabit/ustlink.php");
$gelenYetki =$_SESSION['yetki'];
include("inc/yetkikontrol.php");
$yetki_seviyesi = yetkiKontrol($gelenYetki);
if ($yetki_seviyesi < 10)
{
  header("location:yetkisizerisim.php");
  exit;
}
?>
<!-- Sayfaya özgü linkler -->

<!-- Sayfaya özgü linkler bitiş -->
<?php 
include("sabit/ust.php");
include("sabit/sol.php");
include ("inc/head.php");
 
    
	function writeKullanilabilir($tur_id) {
		$gecmis = new DateTime(); 
		$gecmis ->modify('-6 month'); 
		$gecmis = $gecmis ->format('Y-m-d H:i:s');
		$datakul = [
			'skt' => $gecmis,
			'tur_id' => $tur_id,
		];
		$stmtkul = db::$conn->prepare("select count(id) as toplamsayi from tb_tweet WHERE _deleted =0 and durum = 'Aktif' and skt < :skt and tur_id=:tur_id");
		$stmtkul->execute($datakul);
		$records = $stmtkul->fetch();
		$kullanilabilir = (int)$records['toplamsayi'];
		echo $kullanilabilir;
	}
	function writeToplam($tur_id) {
	  $data = [
			'tur_id' => $tur_id,
		];
		$stmt = db::$conn->prepare("select count(id) as toplamsayi from tb_tweet WHERE _deleted =0 and durum = 'Aktif' and tur_id=:tur_id");
		$stmt->execute($data);
		$records = $stmt->fetch();
		$kullanilabilirToplam = (int)$records['toplamsayi'];
		echo $kullanilabilirToplam;
	}
?>

<!-- İçerik Bölümü -->
<div class="content-wrapper">
    <!-- İçerik Başlık -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Ana sayfa </h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php">Ana sayfa</a></li>
              <li class="breadcrumb-item active">Ana sayfa</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.İçerik Başlık Bitiş -->
    <!-- İçerik Konu  -->
    <section class="content">
        <div class="row">
           <div class="col-12">
            <div class="card card-primary card-tabs">
              <div class="card-header">
					Toplam tweet sayısı
    
              </div>
              <div class="card-body">
				<div class="row">
					<div class="col-sm-5">
						<div class="row">
							<div class="col-sm-3">
							<b>Tür</b>
							</div>
							<div class="col-sm-3">
							<b>Gün</b>
							</div>
							<div class="col-sm-2">
							<b>Saat</b>
							</div>
							<div class="col-sm-2">
							<b>Kullanılabilir</b>
							</div>
							<div class="col-sm-2">
							<b>Toplam</b>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-3">
							Günaydın
							</div>
							<div class="col-sm-3">
							Her gün
							</div>
							<div class="col-sm-2">
							09:00
							</div>
							<div class="col-sm-2">
							<?php writeKullanilabilir(4);?>
							</div>
							<div class="col-sm-2">
							<?php writeToplam(4);?>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-3">
							Karakter
							</div>
							<div class="col-sm-3">
							Her gün
							</div>
							<div class="col-sm-2">
							12:30
							</div>
							<div class="col-sm-2">
							<?php writeKullanilabilir(9);?>
							</div>
							<div class="col-sm-2">
							<?php writeToplam(9);?>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-3">
							Replik
							</div>
							<div class="col-sm-3">
							Her gün
							</div>
							<div class="col-sm-2">
							15:30
							</div>
							<div class="col-sm-2">
							<?php writeKullanilabilir(7);?>
							</div>
							<div class="col-sm-2">
							<?php writeToplam(7);?>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-3">
							Dizi
							</div>
							<div class="col-sm-3">
							Her gün
							</div>
							<div class="col-sm-2">
							18:30
							</div>
							<div class="col-sm-2">
							<?php writeKullanilabilir(8);?>
							</div>
							<div class="col-sm-2">
							<?php writeToplam(8);?>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-3">
							Yeşilçam
							</div>
							<div class="col-sm-3">
							Salı
							</div>
							<div class="col-sm-2">
							20:50
							</div>
							<div class="col-sm-2">
							<?php writeKullanilabilir(6);?>
							</div>
							<div class="col-sm-2">
							<?php writeToplam(6);?>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-3">
							Bilgilendirme
							</div>
							<div class="col-sm-3">
							Çarşamba
							</div>
							<div class="col-sm-2">
							20:50
							</div>
							<div class="col-sm-2">
							<?php writeKullanilabilir(1);?>
							</div>
							<div class="col-sm-2">
							<?php writeToplam(1);?>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-3">
							Tabuu
							</div>
							<div class="col-sm-3">
							Perşembe
							</div>
							<div class="col-sm-2">
							19:30
							</div>
							<div class="col-sm-2">
							<?php writeKullanilabilir(3);?>
							</div>
							<div class="col-sm-2">
							<?php writeToplam(3);?>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-3">
							Etkinlik
							</div>
							<div class="col-sm-3">
							Cuma
							</div>
							<div class="col-sm-2">
							19:30
							</div>
							<div class="col-sm-2">
							<?php writeKullanilabilir(10);?>
							</div>
							<div class="col-sm-2">
							<?php writeToplam(10);?>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-3">
							Süre
							</div>
							<div class="col-sm-3">
							Cumartesi
							</div>
							<div class="col-sm-2">
							15:00
							</div>
							<div class="col-sm-2">
							<?php writeKullanilabilir(2);?>
							</div>
							<div class="col-sm-2">
							<?php writeToplam(2);?>
							</div>
						</div>
							<div class="row">
							<div class="col-sm-3">
							Geceler
							</div>
							<div class="col-sm-3">
							Her gün
							</div>
							<div class="col-sm-2">
							00:30
							</div>
							<div class="col-sm-2">
							<?php writeKullanilabilir(5);?>
							</div>
							<div class="col-sm-2">
							<?php writeToplam(5);?>
							</div>
						</div>
					</div>
				</div>

              </div>
              <!-- /.card -->
            </div>
          </div>
        </div>
    </section>
    <!-- /.İçerik Konu Bitiş -->

</div>
<!-- ./İçerik Bölümü Bitiş -->
<?php 
include("sabit/alt.php");
?>
<!-- Sayfaya özgü scriptler -->

<!-- Sayfaya özgü linkler bitiş -->
<?php
include("sabit/altscript.php");
?>